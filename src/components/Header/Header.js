import React from 'react';
import './Header.scss';

const Header = props => {
  return (
    <header id="header">
      <div className="wrap grid">
        <picture>
          <img src={process.env.PUBLIC_URL + 'img/logo.svg'} />
        </picture>

        <div className="header-searchbar">
          <form>
            <input name="search" placeholder="O que está procurando?" required />
            <button className="header-searchbar--submit">
              <picture>
                <img src={process.env.PUBLIC_URL + 'img/lupa.svg'} title="Pesquisar" />
              </picture>
            </button>
          </form>
        </div>

        <nav className="header-admin flex">
          <a className="flex" href="#" title="Minha Conta">
            <picture>
              <img src={process.env.PUBLIC_URL + 'img/user.svg'} />
            </picture>
            Minha Conta
          </a>
          <a className="flex" href="#" title="Carrinho">
            <picture>
              <img src={process.env.PUBLIC_URL + 'img/cart.svg'} />
            </picture>

            <div className="cart">{ props.cartQty }</div>
          </a>
        </nav>
      </div>
    </header>
  )
}

export default Header;