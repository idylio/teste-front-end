import React, { useState, useEffect } from 'react';
import './Produtos.scss';

import { CarouselProvider, Slider, Slide, ButtonBack, ButtonNext } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';

const API_URL = 'https://corebiz-test.herokuapp.com/api/v1/products';

const Produtos = props => {
  const [produtos, setProdutos] = useState(0)

  useEffect(() => {
    loadProducts()
  }, [])

  const loadProducts = async () => {
    let request = await fetch(API_URL)
    let data = await request.json()
    setProdutos(data)
  }

  const handlePrice = price => {
    price = price.toString()
    return [price.slice(0, price.length - 2), ',', price.slice(price.length - 2)].join('')
  }

  return (
    <section className="products">
      <div className="wrap">
        <h4>Mais Vendidos</h4>

        {produtos && (
          <CarouselProvider
            naturalSlideWidth={100}
            naturalSlideHeight={130}
            infinite={1}
            totalSlides={produtos.length}
            visibleSlides="4">
            
            <ButtonBack><img src={process.env.PUBLIC_URL + '/img/arrow.svg'} /></ButtonBack>
            <Slider>
              {produtos.map((produto, index) => (
                  <Slide index={index} key={index}>
                    <article className="product">
                      {produto.listPrice && (
                        <span className="product-off">OFF</span>
                      )}
                      <picture>
                        <img src={produto.imageUrl} alt={produto.productName} title={produto.productName} />
                      </picture>
                      <div className="product-content">
                        <h2 className="product-title">{produto.productName}</h2>
                        <div className="product-stars">
                          <span className="product-star product-star--full"></span>
                          <span className="product-star product-star--empty"></span>
                        </div>
                        
                        {produto.listPrice && (
                          <p className="product-discount">de R${handlePrice(produto.listPrice)}</p>
                        )}
                        <p className="product-price"><strong>por R$ {handlePrice(produto.price)}</strong></p>
                        {produto.installments.length > 0 && (
                          <p className="product-installments">ou em {produto.installments[0].quantity}x de R${handlePrice(produto.installments[0].value)}</p>
                        )}

                        <button className="product-btn" onClick={() => props.updateCart()}>COMPRAR</button>
                      </div>
                    </article>
                  </Slide>
                ))}
            </Slider>
            <ButtonNext><img src={process.env.PUBLIC_URL + '/img/arrow.svg'} /></ButtonNext>
          </CarouselProvider>
        )}
      </div>
    </section>
  )
}

export default Produtos