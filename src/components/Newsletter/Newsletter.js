import React, { useState } from 'react';
import './Newsletter.scss';

const API_URL = 'https://corebiz-test.herokuapp.com/api/v1/newsletter';

const Newsletter = () => {
  let [hasSubmitted, setHasSubmitted] = useState(0)

  const registerLead = e => {
    e.preventDefault()
    let form = e.target.parentNode
    let formData = new FormData(form)
    
    if( validateInputs(formData) ) {
      let request = fetch(API_URL, {
        headers: {
          'Content-Type': 'application/json',
          'Access-Control-Allow-Origin': '*'
        },
        method: 'POST',
        body: JSON.stringify({
          "email": formData.get('email'),
          "name": formData.get('nome')
        })
      })
  
      e.target.textContent = 'Enviando...'
      request.then(response => response.json())
        .then(json => {
          console.log(json)
          if( json.status !== 'error' ) {
            e.target.textContent = 'Enviado!'
            setHasSubmitted(1)
          } else {
            e.target.textContent = 'Eu quero!'
          }
        })
    }
  }
  
  const validateInputs = form => {
    let hasError = 0
    for(let values of form) {
      let input = document.querySelector(`[name=${values[0]}]`)
      let emailRegex = /\S+@\S+\.\S+/;
  
      // Campo vazio
      if( values[1].length == 0 ) {
        if( values[0] == 'nome' ) {
          input.parentNode.classList.add('error-name')
        } else {
          input.parentNode.classList.add('error-empty')
        }
        hasError++
      } else {
        input.parentNode.classList.remove('error-name')
        input.parentNode.classList.remove('error-empty')
      }
  
      // Validação para email
      if( values[0] == 'email' && !emailRegex.test(values[1]) ) {
        input.parentNode.classList.add('error-email')
        hasError++
      } else {
        input.parentNode.classList.remove('error-email')
      }
    }
  
    return hasError == 0 ? true : false
  }

  return (
    <section className="newsletter">
      <div className="wrap">
        {!hasSubmitted ? (
          <>
            <h5>Participe de nossas news com promoções e novidades!</h5>
            <form className="flex">
              <label>
                <input type="text" name="nome" placeholder="Digite seu nome" required />
              </label>
              <label>
                <input type="email" name="email" placeholder="Digite seu email" required />
              </label>
              <button type="submit" onClick={e => registerLead(e)}>Eu quero!</button>
            </form>
          </>
        ) : (
          <div className="newsletter-submitted">
            <strong>Seu e-mail foi cadastrado com sucesso!</strong>
            <p>A partir de agora você receberá as novidades e ofertas exclusivas.</p>
            <button type="submit" onClick={() => setHasSubmitted(0)}>Cadastrar novo e-mail</button>
          </div>
        )}
      </div>
    </section>
  )
}

export default Newsletter;