import React from 'react';
import './Footer.scss';

const Footer = () => {
  return (
    <footer id="footer">
      <div className="wrap flex">
        <div className="footer-infos">
          <h2>Localização</h2>
          <p>Avenida Andrômeda, 2000. Bloco 6 e 8</p>
          <p>Alphaville SP</p>
          <a href="mailto:brasil@corebiz.ag" title="E-mail">brasil@corebiz.ag</a>
          <a href="tel:+551130901039" title="Telefone">+55 11 3090 1039</a>
        </div>
        <div className="footer-btns flex">
          <a className="footer-btn" href="#" title="Entre em contato">
            <picture>
              <img src={process.env.PUBLIC_URL + '/img/envelope.svg'} />
            </picture>
            <p>Entre em contato</p>
          </a>
          <a className="footer-btn" href="#" title="Fale com o nosso consultor online">
            <picture>
              <img src={process.env.PUBLIC_URL + '/img/headphone.svg'} />
            </picture>
            <p>Fale com o nosso consultor online</p>
          </a>
        </div>
        <div className="footer-madeby flex">
          <a href="#" title="Corebiz">
            Created by
            <picture>
              <img src={process.env.PUBLIC_URL + '/img/corebiz.svg'} title="Corebiz" />
            </picture>
          </a>
          <a href="#" title="VTEX">
            Powered by
            <picture>
              <img src={process.env.PUBLIC_URL + '/img/vtex.svg'} title="VTEX" />
            </picture>
          </a>
        </div>
      </div>
    </footer>
  )
}

export default Footer