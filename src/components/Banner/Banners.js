import React from 'react';
import './Banners.scss';

import { CarouselProvider, Slider, Slide, DotGroup } from 'pure-react-carousel';
import 'pure-react-carousel/dist/react-carousel.es.css';

const bannersUrl = `${process.env.PUBLIC_URL}/img/banners/`;
const bannerImgs = [
  {
    titulo: 'Criar ou migrar seu e-commerce?',
    subtitulo: 'Olá, o que você está buscando?',
    desktop: 'banner-desk.jpg',
    mobile: 'banner-mobile.jpg'
  },
  {
    titulo: 'Criar ou migrar seu e-commerce?',
    subtitulo: 'Olá, o que você está buscando?',
    desktop: 'banner-desk.jpg',
    mobile: 'banner-mobile.jpg'
  },
  {
    titulo: 'Criar ou migrar seu e-commerce?',
    subtitulo: 'Olá, o que você está buscando?',
    desktop: 'banner-desk.jpg',
    mobile: 'banner-mobile.jpg'
  },
  {
    titulo: 'Criar ou migrar seu e-commerce?',
    subtitulo: 'Olá, o que você está buscando?',
    desktop: 'banner-desk.jpg',
    mobile: 'banner-mobile.jpg'
  }
];

const Banners = () => {
  return (
    <section id="banners">
      <CarouselProvider
        naturalSlideWidth={100}
        naturalSlideHeight={22.4}
        totalSlides={ bannerImgs.length }>
          <Slider>
            {bannerImgs.map((img, index) => (
              <Slide index={index} key={index}>
                <div className="carousel__slide--wrap">
                  <div className="carousel__slide--content">
                    <h3>{ img.subtitulo }</h3>
                    <h2>{ img.titulo }</h2>
                  </div>
                </div>
                <picture>
                  <source srcSet={ bannersUrl + img.mobile } media="(max-width: 991px)" />
                  <img src={ bannersUrl + img.desktop } alt={ img.titulo } title={ img.titulo } />
                </picture>
              </Slide>
            ))}
          </Slider>

          <DotGroup />
      </CarouselProvider>
    </section>
  )
}

export default Banners