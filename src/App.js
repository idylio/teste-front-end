import React, { useState, useEffect } from 'react';
import './App.scss';

import Header from './components/Header/Header';
import Banners from './components/Banner/Banners';
import Produtos from './components/Produtos/Produtos';
import Newsletter from './components/Newsletter/Newsletter';
import Footer from './components/Footer/Footer';

function App() {
  let qty = localStorage.getItem('cart') ? localStorage.getItem('cart') : 0
  let [cartQty, setCartQty] = useState(qty)

  const updateCart = () => {
    setCartQty(++cartQty)
    localStorage.setItem('cart', cartQty)
  }

  return (
    <div className="App">
      <Header cartQty={cartQty} />
      <Banners />
      <Produtos updateCart={updateCart} />
      <Newsletter />
      <Footer />
    </div>
  );
}

export default App;
